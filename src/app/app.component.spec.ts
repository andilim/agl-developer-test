import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { PetComponent } from './pet/pet.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { PetService } from './pet/pet.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        PetComponent
      ],
      imports: [
        BrowserModule,
        HttpClientModule
      ],
      providers: [
        PetService
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
