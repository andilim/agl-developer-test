import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { PetComponent } from './pet.component';
import { PetService } from './pet.service';
import { Observable, Observer } from 'rxjs';
import { IOwner, IPet } from './pet.models';
import { List } from 'linqts';

describe('Pet Component', () => {
  let app: PetComponent;
  let fixture: ComponentFixture<PetComponent>;
  let compiled: HTMLDivElement;
  let petService: PetService;
  const mockData: IOwner[] = [{
    name: 'Bob',
    gender: 'Male',
    age: 23,
    pets: [{
      name: 'Garfield',
      type: 'Cat'
    },
    {
      name: 'Fido',
      type: 'Dog'
    }]
  },
  {
    name: 'Jennifer',
    gender: 'Female', age: 18,
    pets: [{ name: 'Garfield', type: 'Cat' }]
  },
  {
    name: 'Steve',
    gender: 'Male',
    age: 45, pets: null
  },
  {
    name: 'Fred',
    gender: 'Male',
    age: 40, pets: [
      { name: 'Tom', type: 'Cat' },
      { name: 'Max', type: 'Cat' },
      { name: 'Sam', type: 'Dog' },
      { name: 'Jim', type: 'Cat' }]
  },
  {
    name: 'Samantha',
    gender: 'Female',
    age: 40, pets: [{
      name: 'Tabby',
      type: 'Cat'
    }]
  },
  {
    name: 'Alice',
    gender: 'Female',
    age: 64, pets: [
      { name: 'Simba', type: 'Cat' },
      { name: 'Nemo', type: 'Fish' }]
  }];

  const listData = new List(mockData);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        HttpClientModule
      ],
      declarations: [
        PetComponent
      ],
      providers: [
        PetService
      ]
    }).compileComponents()
      .then(() => {
        petService = TestBed.get(PetService);
      });
  }));

  it('should load app', () => {
    spyOnPetServiceReturnEmptyResult();
    recreateComponent();
    expect(app).toBeTruthy();
  });

  it('should display genders', () => {
    spyOnPetServiceReturnFullResult();
    recreateComponent();
    const genderHeaders = compiled.querySelectorAll('.gender-header > h2');
    const gender = listData.Select(o => o.gender).Distinct();
    expect(genderHeaders.length).toEqual(gender.Count());
    gender.ForEach((genderText, index) => {
      expect(genderHeaders[index].textContent.trim()).toContain(genderText);
    });
  });

  it('shoud display cats, based on owners gender', () => {
    spyOnPetServiceReturnFullResult();
    recreateComponent();
    const genderHeaders = compiled.querySelectorAll('.gender-header');
    genderHeaders.forEach((container: HTMLDivElement) => {
      const headerElement = container.querySelector('h2');
      const genderText = headerElement.textContent.trim();
      const catsList = new List<IPet>();
      listData.Where(o => o.gender === genderText && o.pets !== null)
        .ForEach((o) => {
          catsList.AddRange(
            new List(o.pets).Where(p => p.type === 'Cat').ToArray()
          );
        });
      const nameListElements = container.querySelectorAll('li');
      expect(nameListElements.length).toEqual(catsList.Count());
      catsList.OrderBy(p => p.name).ForEach((cat, index) => {
        expect(nameListElements[index].textContent.trim()).toContain(cat.name);
      });
    });

  });

  it('should display no record found', () => {
    spyOnPetServiceReturnEmptyResult();
    recreateComponent();
    const norecordElement = compiled.querySelector('.not-found');
    expect(norecordElement).toBeTruthy();
  });

  function spyOnPetServiceReturnEmptyResult() {
    spyOn(petService, 'getOwnersWithPets').and.returnValue(
      Observable.create((observer: Observer<IOwner[]>) => {
        observer.next([]);
        observer.complete();
      })
    );
  }

  function spyOnPetServiceReturnFullResult() {
    spyOn(petService, 'getOwnersWithPets').and.returnValue(
      Observable.create((observer: Observer<IOwner[]>) => {
        observer.next(mockData);
        observer.complete();
      })
    );
  }

  function recreateComponent() {
    fixture = TestBed.createComponent(PetComponent);
    app = fixture.debugElement.componentInstance;
    fixture.detectChanges();
    compiled = fixture.debugElement.nativeElement;
  }
});
