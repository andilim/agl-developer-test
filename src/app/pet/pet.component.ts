import { Component, OnInit } from '@angular/core';
import { PetService } from './pet.service';
import { List } from 'linqts';
import { IOwner } from './pet.models';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'pet',
  templateUrl: 'pet.component.html',
  styleUrls: ['pet.component.scss']
})
export class PetComponent implements OnInit {

  ownersGenders: string[];
  ownerList: List<IOwner>;
  loading = false;
  constructor(private petService: PetService) {

  }

  ngOnInit(): void {
    this.loading = true;
    this.petService.getOwnersWithPets()
      .subscribe((owners) => {
        this.loading = false;
        this.ownerList = new List<IOwner>(owners);
        this.ownersGenders = this.ownerList.Select(o => o.gender).Distinct().ToArray();
      }, (err) => {
        this.loading = false;
        console.error(err);
        this.ownersGenders = [];
      });
  }

  getPetsByOwnerGenderAndType(gender: string, type: string) {
    const result = this.ownerList.Where(o => o.gender === gender && o.pets !== null)
      .SelectMany(o => new List(o.pets).Where(p => p.type === type));
    return result.OrderBy(p => p.name).ToArray();
  }

}
