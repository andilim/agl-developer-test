import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { IOwner } from './pet.models';

@Injectable()
export class PetService {

  constructor(private httpClient: HttpClient) {

  }

  getOwnersWithPets(): Observable<IOwner[]> {
    const epUrl = environment.baseApiUrl + '\people.json';
    return this.httpClient.get(epUrl) as Observable<IOwner[]>;
  }
}
