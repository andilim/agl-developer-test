import { TestBed, async } from '@angular/core/testing';
import { PetService } from './pet.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { IOwner } from './pet.models';

describe('Pet Service', () => {
  let petService: PetService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        HttpClientModule,

      ],
      providers: [
        PetService,
        HttpClient
      ]
    }).compileComponents()
      .then(() => {
        petService = TestBed.get(PetService);
      });
  }));

  it('should return owners with pet', (done) => {
    petService.getOwnersWithPets().subscribe(result => {
      expect(result).toBeTruthy();
      expect((result).length).toBeGreaterThan(0);
      (result).forEach((owner) => {
        if (owner.pets) { expect(owner.pets.length).toBeGreaterThan(0); }
      })
      done();
    });
  });
});
